;;; C-x C-e
;;; パッケージの一覧を更新
;;;   (package-refresh-contents)
;;; 一覧
;;;   (package-list-packages)
;;; インストール
;;;   (package-install 'helm)

;;; リロード
;;; (eval-buffer)

;; バインドキーを調べる
(global-set-key (kbd "C-[ C-d") 'describe-key)
;; コメント
(global-set-key (kbd "C-[ C-c") 'comment-dwim)
(global-set-key (kbd "C-M-|") 'enlarge-window-horizontally)
(global-set-key (kbd "C-[ C-w") 'window-resizer)

(package-initialize)
(setq package-archives
      '(("gnu" . "http://elpa.gnu.org/packages/")
        ("melpa" . "http://melpa.org/packages/")
        ("org" . "http://orgmode.org/elpa/")))

;;; バックアップファイル無し
(setq make-backup-files nil)
(setq auto-save-default nil)
;;; デフォルトの文字コード
(set-default-coding-systems 'utf-8)
;;; 自動で出てくるドキュメントをOFFにする
(global-eldoc-mode -1)

;;; ====================================================================================================
;;; るびきち推奨の初期設定 2014/10/5
;;;   http://emacs.rubikitch.com/sd1407/
;;; 右から左に読む言語に対応させないことで描画高速化
(setq-default bidi-display-reordering nil)
;;; splash screenを無効にする
(setq inhibit-splash-screen t)
;;; 同じ内容を履歴に記録しないようにする
(setq history-delete-duplicates t)
;; C-u C-SPC C-SPC ...でどんどん過去のマークを遡る
(setq set-mark-command-repeat-pop t)
;;; 複数のディレクトリで同じファイル名のファイルを開いたときのバッファ名を調整する
(require 'uniquify)
;; filename<dir> 形式のバッファ名にする
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)
(setq uniquify-ignore-buffers-re "[^*]+")
;;; ファイルを開いた位置を保存する
(require 'saveplace)
(setq-default save-place t)
(setq save-place-file (concat user-emacs-directory "places"))
;;; 釣合う括弧をハイライトする
(show-paren-mode 1)
;;; インデントにTABを使わないようにする
(setq-default indent-tabs-mode nil)
;;; 現在行に色をつける
;(global-hl-line-mode 1)
;;; ミニバッファ履歴を次回Emacs起動時にも保存する
(savehist-mode 1)
;;; シェルに合わせるため、C-hは後退に割り当てる
(global-set-key (kbd "C-h") 'delete-backward-char)
;;; モードラインに時刻を表示する
(display-time)
;;; 行番号・桁番号を表示する
(line-number-mode 1)
(column-number-mode 1)
;;; GCを減らして軽くする
(setq gc-cons-threshold (* 10 gc-cons-threshold))
;;; ログの記録行数を増やす
(setq message-log-max 10000)
;;; 履歴をたくさん保存する
(setq history-length 1000)
;;; メニューバーとツールバーとスクロールバーを消す
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
;;; ====================================================================================================

;;; ====================================================================================================
;;; *** (package-install 'helm)
(require 'helm-config)
(global-set-key (kbd "C-[ C-[") 'helm-mini)
(global-set-key (kbd "C-[ b") 'helm-bookmarks)
(global-set-key (kbd "C-[ i") 'helm-imenu)
(global-set-key (kbd "C-[ k") 'helm-show-kill-ring)

;;; ブックマークジャンプしたときにneotreeのディレクトリも変更する
(defun my-bookmark-jump (candicate)
  (helm-bookmark-jump candicate)
  (neotree-dir default-directory))
(helm-mode 1)
(require 'helm-types)
(setq helm-type-bookmark-actions
      (cons '("Jump and trees to bookmark" . my-bookmark-jump) helm-type-bookmark-actions))
;;; ====================================================================================================

;;; ====================================================================================================
;;; カラーテーマ
(load-theme 'wombat t)
;;; (package-install 'spacemacs-theme)
;(load-theme 'spacemacs-dark t)
;;; ====================================================================================================

;(require 'dired)
(global-set-key (kbd "C-[ B") 'bookmark-set)

;;; *** (package-install 'neotree)
(require 'neotree)
(global-set-key (kbd "C-[ t") 'neotree-dir)

;;; *** (package-install 'markdown-mode)

;;; python-modeはpacmanで
(require 'python-mode)
(setq-default py-split-windows-on-execute-function 'split-window-horizontally)
(define-key python-mode-map (kbd "C-c C-a") 'py-execute-buffer)
(define-key python-mode-map (kbd "C-c C-c") 'py-execute-region)
(define-key python-mode-map (kbd "C-c C-r") 'py-execute-def-or-class)

(defun window-resizer ()
  "Control window size and position."
  (interactive)
  (let ((window-obj (selected-window))
        (current-width (window-width))
        (current-height (window-height))
        (dx (if (= (nth 0 (window-edges)) 0) 1
              -1))
        (dy (if (= (nth 1 (window-edges)) 0) 1
              -1))
        action c)
    (catch 'end-flag
      (while t
        (setq action
              (read-key-sequence-vector (format "size[%dx%d]"
                                                (window-width)
                                                (window-height))))
        (setq c (aref action 0))
        (cond ((= c ?l)
               (enlarge-window-horizontally dx))
              ((= c ?h)
               (shrink-window-horizontally dx))
              ((= c ?j)
               (enlarge-window dy))
              ((= c ?k)
               (shrink-window dy))
              ;; otherwise
              (t
               (let ((last-command-char (aref action 0))
                     (command (key-binding action)))
                 (when command
                   (call-interactively command)))
               (message "Quit")
               (throw 'end-flag t)))))))

(custom-set-variables
 '(package-selected-packages (quote (markdown-mode spacemacs-theme neotree helm))))
(custom-set-faces)

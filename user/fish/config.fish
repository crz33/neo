
# プライベート変数
set -l _private_config_path ~/.config/fish/config_private.fish
if [ -f $_private_config_path ]
   source $_private_config_path
else
   echo "not found"
end

function mkdocs

  #使い方
  set -l usage 'doc [se | fx] [serve | build | upload | clean]'

  #引数の数チェック
  if test (count $argv) -ne 2
    echo 'usage: ' $usage
    return
  end

  #引数展開
  set -l _target $argv[1]
  set -l _command $argv[2]

  #対象チェック
  switch $_target
    case se fx
    case '*'
      echo 'Target' $_target 'is not support!!'
      return
  end

  switch $_command
    case serve
      #ローカルサーバ起動＋自動ビルド
      #docker run --rm -it -v $PWD/$_target:/docs -p 8000:8000 squidfunk/mkdocs-material serve -a 0.0.0.0:8000
      cd ./$_target
      command mkdocs serve -a 0.0.0.0:8000
      cd -
    case build
      #ビルド
      rm -rf $_target/site
      #docker run --rm -it -v $PWD/$_target:/docs squidfunk/mkdocs-material build
      cd ./$_target
      command mkdocs build
      cd -
      sudo chown -R (whoami):wheel $_target/site > /dev/null
    case upload
      #SSHサーバへデータ転送
      sshpass -p $neo_doc_password scp -r -o StrictHostKeyChecking=no ./$_target/site/* "$neo_doc_user"@$neo_doc_server:./$_target.crz33.com/
    case clean
      #めったにやらないがSSHサーバ側のclean
      sshpass -p $neo_doc_password ssh "$neo_doc_user"@$neo_doc_server rm -rf "./$_target.crz33.com/*"
    case '*'
      echo 'Command' $_command 'is not support!!'
    end

end

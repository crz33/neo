function fish_prompt

    if [ $USER = 'root' ]
      set _prox ' # '
    else
      set _prox ' $ '
    end

    printf '\n'(set_color red)$USER(set_color brwhite)'@'(set_color yellow)(prompt_hostname)' '(set_color cyan)(prompt_pwd)'\n%s' $_prox
    
end
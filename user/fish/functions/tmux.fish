function tmux

  #使い方
  set -l usage 'tmux [init | attach | kill | ls]'

  #引数の数チェック
  if test (count $argv) -ne 1
    echo 'usage: ' $usage
    return
  end

  set -l _name 'main'

  switch $argv[1]
    case init
      command tmux new-session -d -s $_name
      command tmux rename-window emacs
      command tmux new-window -n console
      command tmux split-window -h
      #command tmux split-window -v
      command tmux select-pane -t 0
      command tmux select-window -t emacs
      command tmux a -t $_name
    case attach
      command tmux a -t $_name
    case kill
      command tmux kill-session -t $_name
    case ls
      command tmux ls
    case '*'
      echo 'Command' $argv[1] 'is not support!!' 
  end
  
end